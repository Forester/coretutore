package coretutore;

class TestA {

	public static void methodPublic(){
		System.out.println("TestA.methodPublic, calling methodPivate");
		methodPrivate();
	}
	
	protected static void methodProtected(){
		System.out.println("TestA.methodProtected, calling methodPivate");
		methodPrivate();
	}
	
	static void methodDefault(){
		System.out.println("TestA.methodDefault, calling methodPivate");
		methodPrivate();
	}
	
	private static void methodPrivate(){
		System.out.println("TestA.methodPrivate");
	}
}