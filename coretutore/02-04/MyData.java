package coretutore;

public interface MyData {

	
	static boolean isNull (String str) {
		System.out.println("Interface Null Check");
		
		return str== null ? true : "".contentEquals(str) ? true : false;
		
	}
	
	default void print(String str) {
		if (!isNull(str))
			System.out.println("MyData Print::" + str);
	}
}
