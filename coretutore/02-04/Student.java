package coretutore;

public class Student extends Person {

	private String name;

	public Student() {
		System.out.println("Student Created");
	}

	public Student(int i, String n) {
		super(i); // super class constructor called
		this.name = n;
		System.out.println("Student Created with name = " + n);
	}
	
	public static void main(String[] args) {

	System.out.println("For my first trick, a default student:");
	Student st = new Student();
	System.out.println("Moving on, a parametrized student:");
	Student s  = new Student(34, "Hasan");

	}

}
