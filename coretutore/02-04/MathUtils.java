package coretutore;

public class MathUtils {
	
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName (String name) {
		this.name =name;
	}
	
	
//	public static void main(String[] args) {
//		MathUtils mu = new MathUtils();
////		for (int j=10; j>-10; j-- ) {
////			System.out.println(mu.divide(60, j));
////		}
////		System.out.println(mu.add(5, 2));
////		mu.print("Static Method");
////		mu.print("seven", 7);
//		mu.setName("Kriztoffer");
//		System.out.println(mu.getName());
//	}
	
	public int add (int a , int y) {
		return a+y;
	}
	

	public void print(String s) {
		System.out.println(s);
	}
	public void print(String s, int times) {
		for (int i=0; i< times; i++) {
			print(s);
		}
	}

	public int divide(int x, int y) throws IllegalArgumentException {
		if(y==0) throw new IllegalArgumentException("Can't divide by 0");
		return x/y;
	}
	
	


}
