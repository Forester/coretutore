//package com.journaldev.constructor;
package coretutore;

public class Data {

	private String name;

	public Data(String n) {
		System.out.println("Parameterized Constructor");
		this.name = n;
	}

	public String getName() {
		return name;
	}

	public static void main(String[] args) {
		Data d = new Data("Stable4horses");
		System.out.println(d.getName());
	}

}