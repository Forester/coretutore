package coretutore;

public class TestB {

	public static void main(String args[]) /*throws NoAccessException*/ {
		TestA.methodPublic();
		TestA.methodProtected();
		TestA.methodDefault();
//		TestA.methodPrivate();
		
	}
	
	public static void methodPublic(){
		System.out.println("TestB.methodPublic");
	}
	
	protected static void methodProtected(){
		System.out.println("TestB.methodProtected");
	}
	
	static void methodDefault(){
		System.out.println("TestB.methodDefault");
	}
	
	private static void methodPrivate(){
		System.out.println("TestA.methodPrivate");
	}

	
}